
; section data onde declaramos nossas variaveis

section .data:
  msg db 'Hello, world!', 0xa
  len equ $ - msg


; texte onde escrevemos nosso programa 

section .text:

; metodo que sera inicilizado

global _start

_start:
  mov edx, len
  mov ecx, msg
  mov ebx, 1
  mov eax, 4

  ; enviando instrucao para o kernel 

  int 0x80 
; saida
  mov eax, 1
  mov ebx, 0
  int 0x80